
![logo](./public/images/logo.png)


### Package Tracker
====================

### About us

DHL Express tracking - the fastest way to check the status of your package No need to call Customer Service – our online results give you real-time, detailed progress as your package speeds through the DHL network.


### How to run?

Click on below firbase link. 
<https://package-tracking-36f1a.web.app>


### List of dependencies

```JS
npm install react --save
```
```JS
npm install react-dom --save
```

### User Guide
    By using the application you can easily track you package online.


 ![screenshot1](./public/images/1.png)


    You need to enter your parcel id to search parcel details.


 ![screenshot2](./public/images/2.png)


    here is the all details related to your package.


 ![screenshot3](./public/images/3.png)


    You need to enter valid parcel id to track package.


 ![screenshot4](./public/images/4.png)



### Project Structure Diagram


 ![diagram](./public/images/ProjectStructure.png)










