import React, { useState } from 'react'
import { Link } from 'react-router-dom'

export default function Search(props) {
  // State
  const [query, setQuery] = useState('')

  const validate = event => {
    if(query.length <= 0) {
      event.preventDefault();
      alert("Please enter valid parcel id..");
      return;
    }
      return;
 };

 const handleKeypress = e => {
  //it triggers by pressing the enter key
  if (e.key === "Enter") {
    if(query.length <= 0) {
      e.preventDefault();
      alert("Please enter valid parcel id..");
      return;
    }
    else{
      return;
    }
  }
};

  // Render
  return (
  
      <div class="wrap">
      <div class="search">
          <input name="id" type="text" class="searchTerm" onKeyPress={handleKeypress} placeholder="Enter your parcel id here.." value={query} onChange={event => setQuery(event.target.value)}/>
          
          <Link className="searchButton" to={'/results/' + query} onClick={validate} >
            <i class="fa fa-search"></i>
          </Link>
      </div>
    </div>
    
  )
}