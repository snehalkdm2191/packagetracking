// React core
import React from "react";

export default function Card(props) {
  const prop = props.prop;

  // Render
  return (
    //Added for testing purpose
    /* <article className="Card">
        <div className="right">
          <h1 className="parcel id">{prop.parcel_id}</h1>
        </div>
    </article> */

    <div class="info_wrapper">
        <div class="info_container">
        <div class="title_container">
          <h3>Package Details</h3>
        </div>
        <form>
          <div class="row clearfix">
            <div class="col_half">
              <label>ID : </label>
            </div>
            <div class="col_half">
              <label className="lbl-info">{prop.id}</label>
            </div>
          </div>
          <hr/>
          <div class="row clearfix">
            <div class="col_half">
              <label>Parcel ID : </label>
            </div>
            <div class="col_half">
              <label className="lbl-info">{prop.parcel_id}</label>
            </div>
          </div>
          <hr/>
          <div class="row clearfix">
            <div class="col_half">
              <label>User Name : </label>
            </div>
            <div class="col_half">
              <label className="lbl-info">{prop.user_name}</label>
            </div>
          </div>
          <hr/>
          <div class="row clearfix">
            <div class="col_half">
              <label>User Phone : </label>
            </div>
            <div class="col_half">
              <label className="lbl-info">{prop.user_phone}</label>
            </div>
          </div>
          <hr/>
          <div class="row clearfix">
            <div class="col_half">
              <label>Location ID : </label>
            </div>
            <div class="col_half">
              <label className="lbl-info">{prop.location_id}</label>
            </div>
          </div>
          <hr/>
          <div class="row clearfix">
            <div class="col_half">
              <label>Location Name : </label>
            </div>
            <div class="col_half">
              <label className="lbl-info">{prop.location_name}</label>
            </div>
          </div>
          <hr/>
          <div class="row clearfix">
            <div class="col_half">
              <label>Location Coordinates latitude : </label>
            </div>
            <div class="col_half">
              <label className="lbl-info">{prop.location_coordinate_latitude}</label>
            </div>
          </div>
          <hr/>
          <div class="row clearfix">
            <div class="col_half">
              <label>Location Longitude : </label>
            </div>
            <div class="col_half">
              <label className="lbl-info">{prop.location_coordinate_longitude}</label>
            </div>
          </div>
          <hr/>
          <div class="row clearfix">
            <div class="col_half">
              <label>Location status ok : </label>
            </div>
            <div class="col_half">
              <label className="lbl-info">{prop.location_status_ok.toString()}</label>
            </div>
          </div>
          <hr/>
          <div class="row clearfix">
            <div class="col_half">
              <label>ETA : </label>
            </div>
            <div class="col_half">
              <label className="lbl-info">{prop.eta}</label>
            </div>
          </div>
          <hr/>
          <div class="row clearfix">
            <div class="col_half">
              <label>Status : </label>
            </div>
            <div class="col_half">
              <label className="lbl-info">{prop.status}</label>
            </div>
          </div>
          <hr/>
          <div class="row clearfix">
            <div class="col_half">
              <label>Sender : </label>
            </div>
            <div class="col_half">
              <label className="lbl-info">{prop.sender}</label>
            </div>
          </div>
          <hr/>
          <div class="row clearfix">
            <div class="col_half">
              <label>verfication required : </label>
            </div>
            <div class="col_half">
              <label className="lbl-info">{prop.verification_required.toString()}</label>
            </div>
          </div>
          <hr/>
          <div class="row clearfix">
            <div class="col_half">
              <label>Notes : </label>
            </div>
            <div class="col_half">
              <label className="lbl-info">{prop.notes}</label>
            </div>
          </div>
          <hr/>
          <div class="row clearfix">
            <div class="col_half">
              <label>Last Updates : </label>
            </div>
            <div class="col_half">
              <label className="lbl-info">{prop.last_updated}</label>
            </div>
          </div>
          <hr/>
          
          <div class="row clearfix">
            <div>
              <label>Thank you for using service..</label>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}
