// React core
import React from 'react'

// Components
import SearchBar from '../modules/SearchBar'
import Header from '../modules/Header'
import Footer from '../modules/Footer'

export default function HomePage() {

  // Render
  return (
    <section id="main">
        <Header />

        <div className='HomePage'>
          <div className="home-content">
            <span>Track DHL Express Shipments</span>
            <p>Here’s the fastest way to check the status of your shipment. No need to call Customer Service – our online results give you real-time, detailed progress as your shipment speeds through the DHL network.</p>
          </div>

          <SearchBar hasLightTheme={true} />
        </div>

        <Footer />
    </section>
    

  )
}