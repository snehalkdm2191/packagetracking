import React from 'react'

export default function Result() {

  // Render
  return (
        <div className="result-info">
            <h1>No information about your package.</h1>
            <p>Either your parcel is not traceable or you entered an incorrect parcel ID.</p>
        </div>
    )
}