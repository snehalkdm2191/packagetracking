// React core
import React, { useState } from 'react'

// Components
import Card from '../modules/card'
import Result from '../templates/Result'
import Header from '../modules/Header'
import Footer from '../modules/Footer'

export default function Details({ match }) {
  // Data
  const orders = require('../../orders.json')
  //const query = new RegExp(match.params.query, "i")
  const results = orders.filter(id => id.parcel_id === match.params.query)

  // Hooks
  const [cards] = useState(sortCards("parcel_id", results))

  // Render
  return (
    <section id="details">
      <Header />
      
      <div className='DetailsPage'>
        <div className='container'>
          <section className='grid'>
          {results.length <= 0 ? (<Result />) : (cards) }
          </section>
        </div>
      </div> 

      <Footer />
    </section>
  )
}

function sortCards(key, data) {
  const sortedResults = data.sort((a, b) => (a[key] > b[key]) ? 1 : -1)
  const renderedCards = sortedResults.map((item) =>
    <Card key={item.id} prop={item} />
  )

  return renderedCards
}