// React core
import React from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

// Components
import HomePage from './components/templates/HomePage'
import DetailsPage from './components/templates/DetailsPage'

// Style
import './css/style.css'

export default function App() {
  // Render
  return (
    <Router>
      <main className='App'>
        <Switch>
          <Route path='/' exact component={HomePage} />
          <Route path='/results/:query' component={DetailsPage} />
        </Switch>
      </main>
    </Router>
  )
}